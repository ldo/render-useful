'\" t
.TH "RENDER-BATCH" "1" "2025-02-24" "Geek Central" "Render-Useful Collection"
.
.SH NAME
render\-batch \(em batch-mode rendering of
.UR https://blender.org/
Blender
.UE
scenes.
.SH SYNOPSIS
\fBrender\-batch\fI [options ...] blend-file output-file/dir\fR
.SH DESCRIPTION
.PP
.B render\-batch
is a command-line tool that invokes
.UR https://blender.org/
Blender
.UE
in batch mode to render out
a given
.B .blend
file. It can render out a single still frame, or an entire animation.
It can use default settings from the Blender document, or you can override
settings such as resolution, render quality and output format for a particular
invocation. You can also execute arbitrary Python code to change document settings
beyond those that
.B render\-batch
itself knows about.
.
.SH OPTIONS
.
Unless otherwise specified, omitted options default to the values
saved in the
.B .blend
file.
.
.TP
.B \-\-animation
specifies that an animation sequence is to be rendered. If omitted, a
single frame is rendered.
.
For an animation sequence, the
.I output-file/dir
is interpreted as a directory into which to save the rendered frames.
Otherwise, this is the file name to give the single rendered output frame.
.
.TP
\fB\-\-blender=\fIblender\fR
specifies the path to the Blender executable. Defaults to searching for
the name “blender” in your
.BR PATH .
.
.TP
\fB\-\-camera=\fIcamera\fR
specifies the name of the camera to use.
.
.TP
\fB\-\-collections=\fIcollections\fR
specifies the enabling/disabling of specified scene collections.
.I collections
consists of a comma-separated list of one or more
collection names, optionally preceded by “+” or “\-”. “+” indicates to
enable the specified collections (in addition to those already
enabled); “\-” indicates to disable the specified collections, and the
absence of either means the specified collections should be enabled
and all others disabled.
.
.TP
.B \-\-crash\-protect
spawns a separate child process to do the rendering, with automatic
resume after a crash or certain rendering misbehaviour. Currently only
supported together with
.B \-\-animation
and
.BR \-\-existing= ( replace\-all | skip ).
(Best considered experimental for now.)
.
.TP
\fB\-\-digits=\fIdigits\fR
for an animation, the number of digits for formatting frame
numbers to use in names of output frame files. If omitted, defaults
to 4. The generated names will include leading zeroes as necessary to
make up this number of digits, eg
.RB “ 0001.png "”, “" 0002.png "” etc."
.
.TP
.BR \-\-existing=error | overwrite | replace\-all | skip
specifies what to do if an output image file already exists:
.RS
.TP
.B error
\(em signal an error and abort the render
.TP
.B overwrite
\(em overwrite the output file
.TP
.B replace\-all
\(em only for animations: replaces any existing output directory (and its contents)
with the new frames, but only on successful completion of the entire render. Frames
are initially rendered to a temporary directory named
\fIoutput-file/dir\fB\-tmp\fR,
and when all frames are done, the existing
\fIoutput-file/dir\fR
is deleted and
\fIoutput-file/dir\fB\-tmp\fR
renamed to
\fIoutput-file/dir\fR.
If
\fIoutput-file/dir\fB\-tmp\fR
already exists (perhaps as a result of a previous crash), then any existing
frames in it are skipped and rendering continues with frames not yet done.
.TP
.B skip
\(em skip rendering this frame. If rendering an animation, the rendering continues
with any remaining frames.
.PP
If omitted, the default is
.BR \-\-existing=error .
.RE
.
.TP
\fB\-\-format=\fIformat[\fB:\fImode][\fB:\fIdepth]\fR
specifies the format for rendered image files.
.I format
can be \fBPNG\fR, \fBOPENEXR\fR or \fBOPENEXR_MULTILAYER\fR;
.I mode
can be \fBBW\fR, \fBRGB\fR or \fBRGBA\fR, and
.I depth
can be \fB8\fR or \fB16\fR for \fBPNG\fR or \fB16\fR or \fB32\fR for the OpenEXR formats.
.I mode
and
.I depth
may be specified in either order.
.
.TP
\fB\-\-frame=\fIframenr\fR
.TQ
\fB\-\-frames=\fI[[start]\fB,\fI[end][\fB,\fIstep]]\fR
specifies the frame number (if a single frame) or frame range (if an
animation) to be rendered. The start and end frame numbers can also
be specified as the names of timeline markers.
.
.B \-\-frame
and
.B \-\-frames
are synonymous; either one may be used to specify a single frame
or a range.
.
.TP
\fB\-\-only\-view\-layer=\fIview-layer-name\fR
only renders the specified view layer. Also turns off compositing.
.
.TP
\fB\-\-out=\fIoutput-file/dir\fR
alternative way to specify the render destination.
.
.TP
\fB\-\-percent=\fIpercent\fR
specifies the percentage of the render size at which to generate the
images. If omitted, the default is 100 if
.B \-\-resolution
is specified,
otherwise the default is the setting in the
.B .blend
file.
.
.PP
\fB\-\-preexec=\fIscript\fR
.RS
executes the specified Python code prior to the rendering. May be
specified multiple times; the values are concatenated in sequence,
separated by newlines.
.
Note that, to avoid clashes with the namespace of the \fBrender\-batch\fR
program itself, your script is executed within the global context of
a special module object called \fBuserdefs\fR. Initially this module
is empty apart from an automatic import of Blender’s \fBbpy\fR Python
API module, and the \fBexec_text\fR function (described below); any
globals you define will be kept here for subsequent \fB\-\-preexec\fR
calls.
.
If you need more than a few lines of custom script, it can be more
convenient to save them in a text block in the Blender document.
You could execute this with a statement like
.
.RS
.B \-\-preexec="exec(bpy.data.texts[\e"my_custom_script\e"].as_string())"
.RE
.
but a slightly more convenient alternative is to use \fBexec_text\fR,
e.g.:
.
.RS
.B \-\-preexec="exec_text(\e"my_custom_script\e")"
.RE
.
This function can also be called as, e.g.:
.
.RS
.B \-\-preexec="exec_text(\e"my_custom_script\e", \e"my_mod\e")"
.RE
.
This way, your definitions are put into a new module named
\fBmy_mod\fR, which becomes a submodule of \fBuserdefs\fR.
.
The \fBexec_text\fR function is invoked as
.
.RS
\fBexec_text(\fItext_block_name\fB\fI[\fB, \fImodule_name]\fB)\fR
.RE
.
where \fItext_block_name\fR is the name of the text block to execute,
and \fI[module_name]\fR is the name to give a new module containing
any definitions it creates; if omitted, the definitions go straight
into \fBuserdefs\fR.
.RE
.
.TP
\fB\-\-renderer=\fIrenderer\fR
The renderer to use. Values which are recognized by Blender as standard
are “\fBBLENDER_EEVEE\fR“, “\fBBLENDER_WORKBENCH\fR” or “\fBCYCLES\fR”.
.
.TP
\fB\-\-resolution=\fI[width]\fB:\fI[height]\fR
.TQ
\fB\-\-resolution=\fImax-dimen\fR
specifies the dimensions in pixels of each rendered image. If the colon
is present, then at least one of
.B width
and
.B height
must be specified; if one is omitted, it is
calculated from the other so as to maintain the existing aspect
ratio. If there is no colon, then the value is taken as the maximum
dimension for the render, and the other dimension is calculated so as
to maintain the existing aspect ratio.
.
.TP
\fB\-\-renumber=\fIrenumber\fR
for an animation, the starting number to use to generate names for
the output frame files. If omitted, defaults to the frame start
number.
.
.TP
\fB\-\-samples=\fIsamples\fR
the number of samples per pixel (Cycles or Eevee renderers).
.
.TP
\fB\-\-scene=\fIscene\fR
specifies the scene to render.
.
.TP
\fB\-\-stereo\fI[\fB=\fIstereo-parms]\fR
.RS
specifies that a stereoscopic image is to be rendered, using
Blender’s Views feature. If specified,
.I stereo-parms
takes the form
.
.RS
\fIkeyword\fB=\fIvalue[\fB:\fIkeyword\fB=\fIvalue...]\fR
.RE
.
where the valid \fIkeyword\fRs
are
.
.RS
.TS
tab(|) allbox;
c|c|c|c.
full name|alt name(s)|values|default
_
\fBconvergence_distance\fR|\fBcdist\fR|number|document setting
\fBconvergence_mode\fR|\fBcmode\fR|\fBOFFAXIS\fR\[ba]\fBPARALLEL\fR\[ba]\fBTOE\fR|document setting
\fBinterocular_distance\fR|\fBidist\fR, \fBiod\fR|number|document setting
\fBpivot\fR||\fBCENTER\fR\[ba]\fBLEFT\fR\[ba]\fBRIGHT\fR|document setting
\fBside\fR||\fBLEFT\fR\[ba]\fBRIGHT\fR|both sides
.TE
.RE
.
Note the shorter alternate names which may be used instead of the full names.
The full names may also be written with hyphens in place of the underscores.
.
If the \fBside\fR is not specified, then the output file will contain the two
eye images side-by-side, the image for the right eye on the left and vice
versa, for viewing as “cross-eyed” stereo. If the \fBside\fR is specified,
then only the image for that eye is generated.
.RE
.
.TP
\fB\-\-threads=\fIn\fR
how many concurrent threads to use for rendering (defaults to
.RB “ auto ”).
.
.TP
\fB\-\-tile\-size=\fIwidth\fI[\fB:\fIheight]\fR
specifies the size of tiles into which the image is split up to be
processed by concurrent render threads. If only one number if
specified, it is used for both the width and height.
.
.TP
\fB\-\-time\-remap=\fI[old[\fB/\fIold-fps-base]]\fB:\fInew[\fB/\fInew-fps-base]\fR
alters the rendered frame rate to be
\fB(\fInew\fB ÷ \fInew-fps-base\fB) ÷ (\fIold\fB ÷ \fIold-fps-base\fB) × \fR
the existing frame rate, where \fIold\fR and \fInew\fR are positive integers,
and \fIold-fps-base\fR and \fInew-fps-base\fR are positive reals. If \fIold-fps-base\fR
is omitted, then the current FPS base from the \fB.blend\fR file is used.
If \fIold\fR is omitted, then the existing frame rate is used; if \fInew-fps-base\fR
is omitted, then it is set equal to \fIold-fps-base\fR.
If you don’t specify explicit start and end frame numbers, then
the corresponding values set in the \fB.blend\fR file are
automatically remapped to get as close as possible to the same start
and end times, rounded to the nearest whole frame numbers. But note
that the frame step remains unchanged.
.
.TP
.B \-\-trusted
treat the
.B .blend
file as being loaded from a trusted source. This
will enable autoexecution of scripts (e.g. drivers) which might have
been disabled otherwise.
.
.TP
.B \-\-use\-camera\-collections
sets the visible collections to exactly those on which the active
camera is visible.
.
.TP
.B \-\-vse
indicates that the render is the output of the Video Sequence Editor.
That means the presence of a camera in the scene is ignored.
Incompatible with the
.B \-\-camera
and
.B \-\-stereo
options.
.
.SH EXTERNAL PROGRAMS NEEDED
.
The
.B \-\-crash\-protect
option, when used with the Eevee renderer, invokes the
.BR identify (1)
command (part of ImageMagick/GraphicsMagick) to analyze the rendered
image file.
.
.SH NOTES
.
The range of output formats is deliberately kept limited. In particular,
no video output formats are supported. Video encoding is best done as a
separate pass, using a separate purpose-built tool such as
.UR https://ffmpeg.org/
FFmpeg
.UE .
This is because Blender’s rendering is typically time-consuming, and
having to do it over just to change video encoding parameters just
seems like a waste of time.
.
Thus, the supported output formats are
limited to ones that provide
.I lossless
encoding of still frames. It is easy enough to run these through FFmpeg
to encode them to whatever video format you choose, with whatever parameters
you choose. And it is easy enough to experiment with different parameter
settings with different quality/size tradeoffs, all without having to
go through the rendering process again.
.
.SH EXAMPLES
.
.PP
.RS
.B render\-batch test.blend test.png
.RE
.
.PP
Renders the default scene from
.B test.blend
using its default camera and other saved render settings as
a still image, saving the rendered frame into
.BR test.png.
.
.PP
.RS
.nf
.B render\-batch \-\-animation \-\-existing=replace\-all \e
.RS 4
.B \-\-preexec=\(dqbpy.context.scene.view_layers[0].cycles.use_denoising = True\(dq \e
.B test.blend test\-anim
.RE
.fi
.RE
.
.PP
Renders an animation from
.BR test.blend ,
initially saving the frames in
.BR test\-anim\-tmp ,
and then moving that directory to
.B test\-anim
(deleting the latter and its contents if it already exists) when complete. The
Cycles denoiser is enabled for the render (assuming the default renderer is Cycles
and there is only a single view layer to render).
